function fderrs = gradCheck(varargin)
%   gradCheck:
%       Numerically assess whether or not a given function correctly
%       computes a gradient.  Given an initial point and a function handle,
%       gradCheck produces an error plot showing the difference between a
%       computed directional derivative (based on the supplied function's
%       computed gradient at the initial point) and finite difference
%       approximations to it.
%
%       If the user's gradient computation is correct, gradCheck should
%       consistently produce V-shaped plots when calling it many times using
%       the same starting point (since the search direction for doing finite
%       difference approximations are generated randomly on each call).
%       The minimum point on the plots should, on average, reach down to
%       about the square root of the machine precision (though this can
%       vary a bit depending on numerous factors).  Nonetheless, the
%       V-shape should be consistently produced (see the note below
%       regarding the exceptional case of linear functions).
%
%       The idea is that, in exact arithmetic, as the finite difference
%       step size h approaches zero, the error should also decrease to
%       zero.  However, the denominator of the finite difference
%       approximation is h, so in floating point, more and more digits of
%       accuracy are thrown away in the division by h as h becomes tiny in
%       magnitude.  Thus the errors stop decreasing and instead begin to
%       increase more and more, near when h's magnitude falls below the
%       square root of the machine precision.
%
%       A single non-V-shaped plot (even after seeing many V-shaped plots)
%       or a plot only with relatively large errors likely indicates that
%       the gradient computation is not entirely correct.  It is thus
%       necessary to call gradCheck many times over to empirically validate
%       a gradient computation.  One should do multiple calls per starting point
%       and also try different starting points.
%
%       NOTE: for (nearly) linear functions, gradCheck should consistently
%       produce line-shaped plots instead of V-shaped plots, lines which
%       start near zero (machine precision) and linearly increase to
%       approximately one.  This is because, in exact arithmetic, a linear
%       finite difference approximation will exactly match the linear
%       function evaluated at the new test point, but in floating point,
%       this equivalence only holds for relatively large values of h, due
%       to the aforementioned division by h in the denominator.
%
%   USAGE:
%       fderrs = gradCheck(x0,fn);
%       fderrs = gradCheck(x0,fn,fn_number);
%       fderrs = gradCheck(x0,fn,fn_number,fn_set);
%
%   INPUT:
%       x0          [ real-valued column vector ]
%           The finite difference approximations will be based from this
%           point x0.
%
%       fn          [ function handle of single input, must take x0 ]
%           The function that computes a gradient for evaluation.
%
%           [f1,g1,f2,g2,f3,g3,...] = fn(x);
%
%           fn must only take a single argument x, a real-valued column
%           vector specifying the point at which to evaluate a gradient.  fn
%           must return a positive even number of output arguments, in
%           pairs called 'function sets".  A set is defined as fK,gK, where
%           K defines the number of the set (starting with one).  A set can
%           contain different function/gradient pairs.  The jth entry of
%           the column vector fK is the value of the jth function in the
%           Kth set that fn computes. The jth column of matrix gK is the
%           corresponding gradient.
%
%       fn_number   [ positive integer | {1} ]
%           Chooses the jth function to evaluate in a set.  Default is 1
%           if not provided.
%
%       fn_set      [ positive integer | {1} ]
%           Choose the Kth function set, i.e. fK,gK.  Default is 1 if not
%           provided.
%
%   OUTPUT:
%       fderrs      [ real-valued row vector ]
%           The finite difference errors from h = 10^(-1) to 10^(-16).
%
%
%   This routine is based off Michael L. Overton's findif routine for
%   numerically checking gradients.
%
%   gradCheck Version 1.0, 2017, see AGPL license info below.
%
% =========================================================================
% |  gradCheck                                                            |
% |  Copyright (C) 2017 Tim Mitchell                                      |
% |                                                                       |
% |  This program is free software: you can redistribute it and/or modify |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  This program is distributed in the hope that it will be useful,      |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    % smallest finite difference size should go down to machine precision.
    % On 64-bit hardware, ;there's no need to go past 1e-16.
    LIMIT   = 16;

    % process the arguments
    [n,x0,f0,g0,wrapped_fn,fn_number,fn_set] = setup(varargin{:});

    % choose a random direction to perform the finite difference
    d       = rand(n,1) - 0.5;
    g0td    = g0'*d;

    % calculate the finite difference errors in this direction
    fdiffs  = zeros(1,LIMIT);
    fderrs  = zeros(1,LIMIT);
    for k = 1:LIMIT
        h           = 10^(-k);
        xpert       = x0 + h*d;
        fpert       = wrapped_fn(xpert);
        fdiffs(k)   = (fpert-f0)/h;
        fderrs(k)   = abs(fdiffs(k) - g0td);
    end

    % plot them - a correct gradient show produce a V-like plot, going down
    % to about half of machine precision.
    figure()
    semilogy(fderrs,'*');

    title({ 'gradCheck Finite Difference Plot',                     ...
            sprintf('Function: %d ------ Set: %d',fn_number,fn_set) });

    ylabel('Finite Difference Error')

    xlim([0 LIMIT+1]);
    set(gca,'XTick',1:LIMIT);
    set(gca,'xticklabel',-(1:LIMIT));
    xlabel('Finite Difference Size h = 10^x');
end

function [n,x0,f0,g0,wrapped_fn,fn_num,fn_set] = setup(x0,fn,fn_num,fn_set)

    % sanity checks on the inputs from the user
    assert(size(x0,2) == 1,'x must have only one column.');
    assert(isa(fn,'function_handle'),'fn must be a function handle.');

    if nargin < 3
        fn_num = 1;
    else
        assert(fn_num >= 1,'fn_number must be a positive integer.');
    end

    if nargin < 4
        fn_set = 1;
    else
        assert(fn_set >= 1,'fn_set must be a positive integer.');
    end

    n           = length(x0);
    args        = cell(1,2*fn_set);
    [f0,g0]     = wrapper(x0);
    wrapped_fn  = @wrapper;

    if isinf(f0) || isnan(f0)
        error('initial f(x) value must not be inf or nan.')
    end
    if size(g0) ~= n
        error('size of gradient is wrong')
    end

    function [f,g] = wrapper(x)

        % if this fails, then one of the following may have caused it:
        % - fn_set was set too high by the user
        % - the user's function is not implemented correctly
        % - x is the wrong dimension or isn't real-valued
        [args{:}]   = fn(x);

        % get the requested set of values and gradients
        set_indx    = (2*fn_set) - 1;
        F           = args{set_indx};
        G           = args{set_indx+1};

        % get the request function number in that set
        % if this fails, the user set fn_number to high
        f           = F(fn_num);
        g           = G(:,fn_num);

    end
end
